# gitlab-ci-hugo

Static site generated using Hugo and deployed to Gitlab Pages using Gitlab CI.

- Builds are triggered on pushes to `main` branch.

- This solution is built on the [hugo build template provided by gitlab](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Pages/Hugo.gitlab-ci.yml)

- In the `.gitlab-ci.yml` file, we use a script to check the minutes of the current time in UTC
    - if mins < 30 then it executes hugo build successfully
    - else exit with code 1 (fail build)
